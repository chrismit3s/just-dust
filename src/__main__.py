from argparse import ArgumentParser
from os import remove
from random import random, choice
from src import PARTICLE, PROTON, NEUTRON, ELECTRON
from src import Particle, Universe, Canvas
import numpy as np


def random_vec(vec):
    return (2 * np.random.rand(2) - 1) * vec

def symmetric_setup(universe, num_particles, max_mass):
    pos_range = universe.canvas.size * 0.6
    vel_range = universe.canvas.size * (0.01, 0.14)

    # for uneven particle count, one stationary particle in the center
    if num_particles % 2 == 1:
        universe.particles.append(Particle(
            pos=(0, 0),
            vel=(0, 0),
            mass=max_mass * random() / num_particles,
            **PARTICLE))

    # add two particles on opposite sides
    for _ in range(num_particles // 2):
        pos = random_vec(pos_range)
        vel = random_vec(vel_range)
        mass = max_mass * random() / num_particles
        for coeff in [-1, 1]:
            universe.particles.append(Particle(
                pos=coeff * pos,
                vel=coeff * vel,
                mass=mass,
                **PARTICLE))

def natural_setup(universe, num_particles):
    pos_range = universe.canvas.size * 0.8
    vel_range = universe.canvas.size * 0.1

    for _ in range(num_particles):
        universe.particles.append(Particle(
            pos=random_vec(pos_range),
            vel=random_vec(vel_range),
            **choice([PROTON, NEUTRON, ELECTRON])))

def random_setup(universe, num_particles, max_mass):
    pos_range = universe.canvas.size * 0.8
    vel_range = universe.canvas.size * 0.1

    for _ in range(num_particles):
        universe.particles.append(Particle(
            pos=random_vec(pos_range),
            vel=random_vec(vel_range),
            mass=max_mass * random() / num_particles,
            **PARTICLE))


if __name__ == "__main__":
    # setup args
    parser = ArgumentParser()

    parser.add_argument("-N", "--particle-count",
                        dest="num_particles",
                        type=int,
                        default=50,
                        help="the number of particles in the simulation")
    parser.add_argument("-M", "--max-mass",
                        dest="max_mass",
                        type=int,
                        default=1_000,
                        help="the maximum mass of all particles combined, ignored when -n is set")

    parser.add_argument("--fps",
                        type=float,
                        default=30.0,
                        help="the (maximum) framerate")
    parser.add_argument("--res",
                        type=str,
                        default="720x720",
                        help="the resolution")
    parser.add_argument("--preview-only",
                        dest="preview_only",
                        action="store_true",
                        help="dont save the video, just show the preview")

    group = parser.add_mutually_exclusive_group()
    group.add_argument("-s", "--symmetric",
                       dest="mode",
                       action="store_const",
                       const="symmetric",
                       help="start with a point symmetric particle set")
    group.add_argument("-n", "--natural",
                       dest="mode",
                       action="store_const",
                       const="natural",
                       help="use the natural particles (neutrons, protons, electrons)")
    group.add_argument("-r", "--random",
                       dest="mode",
                       action="store_const",
                       const="random",
                       help="start with a random distribution")
    parser.set_defaults(mode="random")

    args = parser.parse_args()

    # run simulation
    with Canvas(args.res.split("x"), args.fps) as canvas:
        u = Universe(canvas, method="leapfrog")

        # random, but symmetric setup
        if args.mode == "symmetric":
            symmetric_setup(u, args.num_particles, args.max_mass)

        # natural collapse
        elif args.mode == "natural":
            natural_setup(u, args.num_particles)

        # random collapse
        elif args.mode == "random":
            random_setup(u, args.num_particles, args.max_mass)

        u.loop()
        
    # delete file if wanted
    if args.preview_only or input("Save video? (Y/n) ").lower() == "n":
        remove(canvas.filename)


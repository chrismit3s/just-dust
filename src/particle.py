from math import isclose
from random import random
import numpy as np


def _compare(a, b):
    type_a = type(a)
    if type_a is not type(b):
        return False

    if type_a == float:
        return isclose(a, b)
    elif type_a == np.ndarray:
        return np.allclose(a, b)
    else:
        return (a == b)


class Particle():
    def __init__(self, pos, vel, mass=1.0, charge=0.0, can_merge=True, color=(255, 255, 255), shape="["):
        # drawing related properties
        self.color = color
        self.shape = shape.lower() if (shape.lower() in {"[", "square", "(", "circle", ".", "dot"}) else "["
        
        # physical properties
        self.pos = np.array(pos, dtype="float64")
        self.vel = np.array(vel, dtype="float64")
        self.mass = float(mass)
        self.charge = float(charge)
        self.can_merge = bool(can_merge)

    def __eq__(self, other):
        return all(_compare(getattr(self, attr),
                            getattr(other, attr))
                   for attr
                   in ["pos", "vel", "mass", "charge", "color", "shape", "can_merge"])

    def __str__(self):
        return f"P{tuple(self.pos)}"

    def __repr__(self):
        return f"[P{tuple(self.pos)} V{tuple(self.vel)} M{float(self.mass):.1f} C{float(self.charge):.1f}]"

    @property
    def width(self):
        if self.shape == ".":
            return 1
        elif self.mass < 2.0:
            return 5
        else:
            return self.mass ** 0.5

    def draw(self, canvas):
        if self.shape in {"[", "square"}:
            canvas.draw_rect(self.pos, self.width, self.color)
        elif self.shape in {"(", "circle", "round"}:
            canvas.draw_circle(self.pos, self.width / 2, self.color)
        elif self.shape in {".", "dot", "point", "pixel"}:
            canvas.draw_pixel(self.pos, self.color)

    def tick(self, dt):
        self.pos += dt * self.vel

    def accelerate(self, dt, a):
        self.vel += dt * a

    def apply_force(self, dt, force):
        self.accelerate(dt, force / self.mass)

    def distance_to(self, p):
        return np.linalg.norm(self.pos - p.pos)

    def can_merge_with(self, p):
        return self.can_merge and p.can_merge

    def collide_with(self, p):
        """
        runs all necessary checks and collides the two particles
        if possible; returns True when the two particles merged,
        False else
        """
        # check if they're close enough to collide
        if self.distance_to(p) < (self.width + p.width) / 2:
            if self.can_merge_with(p):  # inelastic (merge)
                self.inelastic_collision(p)
                return True  # merged
            else:  # elastic
                print("elastic collision")
                self.elastic_collision(p)
                return False  # no merge
        return False  # nothing happened

    def inelastic_collision(self, p):
        total_momentum = p.vel * p.mass + self.vel * self.mass
        total_mass = p.mass + self.mass

        # weighted average for position after merge
        self.pos = (self.pos * self.mass + p.pos * p.mass) / total_mass

        # see https://www.wikiwand.com/en/Inelastic_collision
        self.mass = total_mass
        self.vel = total_momentum / total_mass

    def elastic_collision(self, p):
        dpos = p.pos - self.pos
        dvel = p.vel - self.vel
        dist = np.linalg.norm(dpos)

        # see https://en.wikipedia.org/wiki/Elastic_collision
        x = 2 / (self.mass + p.mass) * np.dot(dvel, dpos) / (dist ** 2) * dpos
        self.vel += p.mass * x
        p.vel -= self.mass * x

from os.path import join


OUT_DIR = join(".", "out", "")  # empty string so filename can just be appended
SCALE = 1.0  # in px/unit

# see coolors.co/0e0f29-c8dcc8-b9172f-255c99-bfc0c0
PALETTE = {
    "background": (0x29, 0x0F, 0x0E),
    "particle":   (0xC8, 0xDC, 0xC8),
    "proton":     (0x2F, 0x17, 0xB9),
    "electron":   (0x99, 0x5C, 0x25),
    "neutron":    (0xC0, 0xC0, 0xBF)}

PARTICLE = {"shape": "(",
            "can_merge": True,
            "color": PALETTE["particle"]}
PROTON =   {**PARTICLE,
            "can_merge": False,
            "mass":    50.0,
            "charge":  1.0,
            "color":   PALETTE["proton"]}
NEUTRON =  {**PARTICLE,
            "can_merge": False,
            "mass":    50.0,
            "charge":  0.0,
            "color":   PALETTE["neutron"]}
ELECTRON = {**PARTICLE,
            "can_merge": False,
            "mass":    0.0001,
            "charge": -1.0,
            "color":   PALETTE["electron"]}


from src.particle import Particle
from src.universe import Universe
from src.canvas import Canvas

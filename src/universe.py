from itertools import combinations
from numpy.linalg import norm
from random import random
from src import Particle
from src import PALETTE
import numpy as np


class Universe():
    def __init__(self, canvas, particles=[], method="euler"):  # particles as a mutable argument is okay, it just gets iterated over (not stored in any way)
        # select tick method
        self.tick = {
            "euler":       self.euler_step,
            "leapfrog":    self.leapfrog_step,
            "runge-kutta": self.rungekutta_step}.get(method)

        # drawing related properties
        self.canvas = canvas

        # set physical properties
        self.grav_const = min(self.canvas.size) * 1.5
        self.elecmag_const = min(self.canvas.size) * 0.02

        # particle init
        self.particles = []
        for p in particles:
            p.canvas = self.canvas
            self.particles.append(p)

    def draw(self):
        self.canvas.fill(PALETTE["background"])
        for p in self.particles:
            p.draw(self.canvas)
        self.canvas.update()

    def euler_step(self, dt):
        # update velocities
        merged_particles = []
        for p1, p2 in combinations(self.particles, r=2):
            # check for a collision + merge
            if p1.collide_with(p2):
                merged_particles.append(p2)

            # else calculate gravitational + electromagnetic influence
            else:
                force = self.force_between(p1, p2)
                p1.apply_force(dt, +force)
                p2.apply_force(dt, -force)

        # remove merged particles
        for p in merged_particles:
            self.particles.remove(p)

        # update positions
        for p in self.particles:
            p.tick(dt)

    def leapfrog_step(self, dt):
        # move particles half a timestep
        for p in self.particles:
            p.tick(dt / 2)

        # update velocities
        merged_particles = []
        for p1, p2 in combinations(self.particles, r=2):
            # check for a collision + merge
            if p1.collide_with(p2):
                merged_particles.append(p2)

            # else calculate gravitational + electromagnetic influence
            else:
                force = self.force_between(p1, p2)
                p1.apply_force(dt, +force)
                p2.apply_force(dt, -force)

        # remove merged particles

        # the self.particles.remove sometimes fails, and I have no fucking clue why
        # its because somehow a particle in merged_particles isnt contained in
        # self.particles, which should be impossible, and I though I fixed it with the
        # __eq__ magic method in Particle, but well that didnt help
        try:
            prev = None
            for p in merged_particles:
                prev = p
                self.particles.remove(p)
        except ValueError:
            with np.printoptions(formatter={"float": "{:+6.2f}".format}):
                print("\n".join(repr(p) for p in self.particles), repr(prev), prev in self.particles, sep="\n")
            raise

        # move particles half a timestep
        for p in self.particles:
            p.tick(dt / 2)

    def rungekutta_step(self, dt):
        r = np.zeros(shape=(4, len(self.particles), 2), dtype="float64")
        v = np.zeros(shape=(4, len(self.particles), 2), dtype="float64")
        a = np.zeros(shape=(4, len(self.particles), 2), dtype="float64")

        # calculate v0 and a0 (initial values)
        for i, p in enumerate(self.particles):
            r[0, i] = p.pos
            v[0, i] = p.vel
            a[0, i] = self.force_on(p) / p.mass

        ##TODO##
        raise NotImplementedError

        # calculate v1 and a1 (half a timestep later)
        for i, p in enumerate(self.particles):
            p.tick(dt / 2)
            v[1, i] = v[0, i] + a[0, i] * dt / 2
            a[1, i] = 0#

        # update velocities
        merged_particles = []
        for p1, p2 in combinations(self.particles, r=2):
            # check for a collision + merge
            if p1.collide_with(p2):
                merged_particles.append(p2)

            # else calculate gravitational + electromagnetic influence
            else:
                force = self.force_between(p1, p2)
                p1.apply_force(dt, +force)
                p2.apply_force(dt, -force)

        # remove merged particles
        for p in merged_particles:
            self.particles.remove(p)

        # move particles half a timestep
        for p in self.particles:
            p.tick(dt / 2)

    def force_between(self, p1, p2):
        diff = p2.pos - p1.pos
        dist = norm(diff)
        return diff * (self.grav_const * p1.mass * p2.mass - self.elecmag_const * p1.charge * p2.charge) / (dist ** 3)  # ** 3 to normalize diff and just use its direction

    def force_on(self, p):
        force = np.zeros(shape=(2,), dtype="float64")
        for other in self.particles:
            if other != p:
                force += self.force_between(p, other)
        return force

    def loop(self):
        while self.canvas.is_open():
            self.draw()
            self.tick(1 / self.canvas.fps)

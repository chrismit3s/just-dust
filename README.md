# just-dust

Dust-like particles acting under the gravitational pull of each other. Just dust in free space ;)

It's a remake of the [particle game](https://gitlab.com/chrismit3s/particle-game) I made some time ago. This time with a canvas wrapper and export as video functionality.

![example video](./out/examples/400-particles-stable-orbit.webm)

## Setup

Just clone this repo, create a venv, install the packages and run it.

> _Note: You need python>=3.8, as this project uses the [walrus operator](https://docs.python.org/3/whatsnew/3.8.html#assignment-expressions)._

On linux:

    git clone https://gitlab.com/chrismit3s/just-dust
    cd just-dust
    python3.8 -m venv venv
    source ./venv/bin/activate
    python -m pip install -r requirements.txt

On windows (with [py launcher](https://docs.python.org/3/using/windows.html) installed):

    git clone https://gitlab.com/chrismit3s/just-dust
    cd just-dust
    py -3.8 -m venv venv
    .\venv\scripts\activate.bat
    python -m pip install -r requirements.txt

## Usage

Just run the `src` module:

    python -m src [-h] [-N NUM_PARTICLES] [-M MAX_MASS] [--fps FPS] [--res RES] [--preview-only] [-s | -n | -r]

| Argument             | Explanation |
|:---------------------|:------------|
| -h, --help           | show help message and exit |
| -N, --particle-count | the number of particles in the simulation |
| -M, --max-mass       | the maximum mass of all particles combined, ignored when -n is set |
| --fps                | the (maximum) framerate |
| --res                | the resolution |
| --preview-only       | dont save the video, just show the preview |
| -s, --symmetric      | start with a point symmetric particle set |
| -n, --natural        | use the natural particles (neutrons, protons, electrons) |
| -r, --random         | start with a random distribution |
